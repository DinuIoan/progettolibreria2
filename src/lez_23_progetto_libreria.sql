DROP DATABASE IF EXISTS homework;
CREATE DATABASE homework;
USE homework;

create table libro (
	ID_libro INTEGER PRIMARY KEY AUTO_INCREMENT,
	CodiceUnivoco VARCHAR(20) UNIQUE,
	Titolo varchar(50) NOT NULL
);

INSERT INTO libro (CodiceUnivoco, Titolo) VALUES
("AB123456", "Il signore degli anelli"),
("BC123456", "Il vecchio e il mare"),
("CD123456", "Eragon"),
("DE123456", "Se questo è un uomo"),
("EF123456", "La divina commedia"),
("FG123456", "Introduzione agli algoritmi");

create table autore (
	ID_autore  integer(10)  PRIMARY KEY auto_increment,
	Nome varchar(20) not null,
	Cognome varchar(20) not null
);

INSERT INTO autore (Nome, Cognome) VALUES
("John", "Tolkien"),
("Ernest", "Hemingway"),
("Christopher", "Paolini"),
("Primo", "Levi"),
("Dante", "Alighieri"),
("Thomas", "Cormen"),
("Clifford", "Stein");

create table categoria (
	ID_categoria integer(10)  PRIMARY KEY auto_increment,
	nomecategoria varchar(20) not null
);

INSERT INTO categoria (nomecategoria) VALUES
("Fantasy"),
("Classici"),
("Letteratura"),
("Informatica");

create table autore_libro (
	ID_autore integer(10)   not null,
	ID_libro integer(10)   not null,
	PRIMARY KEY(ID_autore, ID_libro),
	FOREIGN KEY (ID_autore) REFERENCES autore(ID_autore),
	FOREIGN KEY (ID_libro) REFERENCES libro(ID_libro)
);

INSERT INTO autore_libro (ID_libro, ID_autore) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6);


create table libro_categoria (
	ID_categoria integer(10) not null,
	ID_libro integer(10) not null,
	PRIMARY KEY(ID_categoria, ID_libro),
	FOREIGN KEY (ID_categoria) REFERENCES categoria(ID_categoria),
	FOREIGN KEY (ID_libro) REFERENCES libro(ID_libro)
);

INSERT INTO libro_categoria (ID_libro, ID_categoria) VALUES
(1, 1),
(2, 2),
(3, 1),
(4, 2),
(5, 2),
(5, 3),
(6, 4);

create table iscritto (

	ID_iscritto integer(10)  PRIMARY KEY auto_increment,
	Nome varchar(20) not null,
	Cognome varchar(20) not null,
	cf varchar(16) unique not null,
	idTessera varchar(20) unique not null

);
INSERT INTO iscritto(Nome, Cognome, cf, idtessera) VALUES
("Mario", "Rossi", "CF23456", "ID1682347"),
("Pino", "Hacker", "CF892316", "ID352147"),
("Laura", "Verdi", "CF1456", "ID1621147"),
("Giulia", "Bianchi", "CF559977", "ID987347");


create table prestito (
	DataInizio datetime not null, 
	DataFine datetime,
	ID_iscritto integer(10) not null,
	ID_libro integer(10) not null,
	num_prestito VARCHAR(12) PRIMARY KEY NOT NULL UNIQUE,
	
	FOREIGN KEY (ID_libro) REFERENCES libro(ID_libro),
	FOREIGN KEY (ID_iscritto) REFERENCES iscritto(ID_iscritto)
);

INSERT INTO prestito(ID_libro, ID_iscritto, num_prestito, DataInizio, DataFine) VALUES
(1, 1, "PR123456", "2008-02-01", null),
(2, 3, "PR234567", "2009-02-01", "2009-03-01"),
(3, 3, "PR345678", "2010-02-01" , null),
(4, 2, "PR456789", "2011-02-01" , null);




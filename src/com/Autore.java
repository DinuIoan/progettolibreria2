package com;

import java.util.ArrayList;

public class Autore {
	private int idAutore;
	private String nome;
	private String cognome;
	private ArrayList<Libro> elencoLibri = new ArrayList<Libro>();
	Autore(){
		
	}
	
	public int getIdAutore() {
		return idAutore;
	}
	public void setIdAutore(int idAutore) {
		this.idAutore = idAutore;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	public String dettagliAutore() {
		String dettagli = this.nome + ", " +this.cognome;
		return dettagli;
	}
	
	
}

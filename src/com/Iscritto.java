package com;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Iscritto {
	private int idIscritto;
	private String nome;
	private String cognome;
	private String codFiscale;
	private String idTessera;
	
	Iscritto(){

	}

	public int getIdIscritto() {
		return idIscritto;
	}

	public void setIdIscritto(int idIscritto) {
		this.idIscritto = idIscritto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCodFiscale() {
		return codFiscale;
	}

	public void setCodFiscale(String codFiscale) {
		this.codFiscale = codFiscale;
	}

	public String getIdTessera() {
		return idTessera;
	}

	public void setIdTessera() {
		this.idTessera = this.nome.substring(0, 3) + this.cognome.substring(0, 3) + LocalDate.now().toString().replace("-", "");
	}
	
	public String dettagliIscritto() {
		String dettagli = this.nome + ", " +this.cognome + ", " +this.codFiscale + ", " +this.idTessera;
		return dettagli;
	}
}

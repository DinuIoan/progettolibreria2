package com;

import java.util.ArrayList;

public class Categoria {
	private int idCategoria;
	private String nomeCategoria;
	private ArrayList<Libro> elencoLibri = new ArrayList<Libro>();
	
	Categoria(){
		
	}

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getNomeCategoria() {
		return nomeCategoria;
	}

	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}
	
	public String dettagliCAtegoria() {
		String dettagli = this.idCategoria + ", " + this.nomeCategoria;
		return dettagli;
	}
}

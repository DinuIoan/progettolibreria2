package com;

import java.util.ArrayList;

public class Libro {
	private int idLibro;
	private String codiceUnivoco;
	private String titolo;
	private ArrayList<Autore> elencoAutori = new ArrayList<Autore>();
	private ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();
	
	Libro(){
		
	}

	public int getIdLibro() {
		return idLibro;
	}

	public void setIdLibro(int idLibro) {
		this.idLibro = idLibro;
	}

	public String getCodiceUnivoco() {
		return codiceUnivoco;
	}

	public void setCodiceUnivoco(String codiceUnivoco) {
		this.codiceUnivoco = codiceUnivoco;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public ArrayList<Autore> getElencoAutori() {
		return elencoAutori;
	}

	public void setElencoAutori(Autore autore) {
		this.elencoAutori.add(autore);
	}

	public ArrayList<Categoria> getElencoCategorie() {
		return elencoCategorie;
	}

	public void setElencoCategorie(ArrayList<Categoria> elencoCategorie) {
		this.elencoCategorie = elencoCategorie;
	}
	
	
}

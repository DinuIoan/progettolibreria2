package com;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Prestito {
	private ArrayList<Libro> elencoLibri = new ArrayList<Libro>();
	private Iscritto iscritto = null;
	private String numeroPrestito;
	private LocalDateTime dataInizio;
	private LocalDateTime dataFine = null;
	
	Prestito(){
		this.setDataInizio();
	}
	
	public ArrayList<Libro> getElencoLibri() {
		return elencoLibri;
	}
	public void setElencoLibri(ArrayList<Libro> elencoLibri) {
		this.elencoLibri = elencoLibri;
	}
	public Iscritto getIscritto() {
		return iscritto;
	}
	public void setIscritto(Iscritto iscritto) {
		this.iscritto = iscritto;
	}
	public String getNumeroPrestito() {
		return numeroPrestito;
	}
	public void setNumeroPrestito(String numeroPrestito) {
		this.numeroPrestito = numeroPrestito;
	}
	public LocalDateTime getDataInizio() {
		return dataInizio;
	}
	public void setDataInizio() {
		this.dataInizio = LocalDateTime.now();
	}
	public LocalDateTime getDataFine() {
		return dataFine;
	}
	public void setDataFine(LocalDateTime dataFine) {
		this.dataFine = dataFine;
	}

	@Override
	public String toString() {
		return "Prestito [elencoLibri=" + elencoLibri + ", iscritto=" + iscritto + ", numeroPrestito=" + numeroPrestito
				+ ", dataInizio=" + dataInizio + ", dataFine=" + dataFine + "]";
	}
	
	
	
}

package com;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.UUID;

public class Controller {
	private ArrayList<Iscritto> listaIscritti = new ArrayList<Iscritto>();
	private ArrayList<Categoria> listaCategorie = new ArrayList<Categoria>();
	private ArrayList<Libro> listaLibri = new ArrayList<Libro>();
	private ArrayList<Prestito> listaPrestiti = new ArrayList<Prestito>();
	private ArrayList<Autore> listaAutori = new ArrayList<Autore>();

	public boolean insertIscritto(String nome, String cognome, String codiceFiscale) {
		try {
			if (!addIscritto(nome, cognome, codiceFiscale))
				return false;

		} catch (SQLException exception) {
			System.out.println(exception.getMessage());
			return false;
		}
		return true;
	}

	public void setAll() {
		try {
			this.setLibri();
			this.setAutori();
			this.setCategorie();
			this.setIscritti();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean inserLibro(String titolo, String autori) {
		String[] elencoAutori = autori.trim().split(",");
		String codiceUnivoco = UUID.randomUUID().toString().replace("-", "").substring(0, 20).toUpperCase();
		try {
			if (!addLibro(titolo, codiceUnivoco, elencoAutori))
				return false;
		} catch (SQLException exception) {
			System.out.println(exception.getMessage());
			return false;
		}
		return true;
	}

	public boolean inserAutore(String nome, String cognome) {
		try {
			if (!addAutore(nome, cognome))
				return false;
		} catch (SQLException exception) {
			System.out.println(exception.getMessage());
			return false;
		}
		return true;
	}

	public boolean inserCategoria(String nomeCategoria) {
		try {
			if (!addCategoria(nomeCategoria))
				return false;
		} catch (SQLException exception) {
			System.out.println(exception.getMessage());
			return false;
		}
		return true;
	}

	public boolean inserPrestito(Iscritto iscritto, ArrayList<Libro> libriSelezionati) {
		String numeroPrestito = UUID.randomUUID().toString().replace("-", "").substring(0, 12).toUpperCase();
		try {
			if (!addPrestito(iscritto, numeroPrestito, libriSelezionati))
				return false;
		} catch (SQLException exception) {
			System.out.println(exception.getMessage());
			return false;
		}
		return true;
	}

	public boolean eliminazioneIscritto(String codiceFiscale) {
		this.setAll();

		for (int i = 0; i < this.listaIscritti.size(); i++) {
			Iscritto temp = this.listaIscritti.get(i);

			if (temp.getCodFiscale().equals(codiceFiscale)) {
				try {
					if (eliminaIscritto(codiceFiscale)) {
						return true;
					}
					else {
						return false;
					}
				} catch (SQLException e) {
					e.printStackTrace();
					return false;
				}
			}
		}
		return false;
	}

	private boolean addIscritto(String nome, String cognome, String codiceFiscale) throws SQLException {
		Iscritto iscritto = new Iscritto();
		iscritto.setNome(nome);
		iscritto.setCognome(cognome);
		iscritto.setCodFiscale(codiceFiscale);
		iscritto.setIdTessera();

		Connection conn = Connector.getIstance().getConnection();
		String queryInserimento = "INSERT INTO iscritto (Nome,Cognome,cf,idTessera) VALUE (?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(queryInserimento, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, iscritto.getNome());
		ps.setString(2, iscritto.getCognome());
		ps.setString(3, iscritto.getCodFiscale());
		ps.setString(4, iscritto.getIdTessera());
		ps.executeUpdate();
		ResultSet result = ps.getGeneratedKeys();
		result.next();
		iscritto.setIdIscritto(result.getInt(1));

		if (iscritto.getIdIscritto() > 0)
			return true;
		else
			return false;
	}

	private boolean addLibro(String titolo, String codice, String[] elenco) throws SQLException {
		this.setAll();

		Libro libro = new Libro();
		libro.setCodiceUnivoco(codice);
		libro.setTitolo(titolo);
		for(int i = 0; i < elenco.length; i++) {
			String[] autore = elenco[i].split(" ");
			Autore autoreTemp = new Autore();
			autoreTemp.setNome(autore[0]);
			autoreTemp.setCognome(autore[1]);
			for(int k = 0; k < listaAutori.size(); k++) {
				if(listaAutori.get(k).getNome().equals(autoreTemp.getNome()) && listaAutori.get(k).getCognome().equals(autoreTemp.getCognome())) {
					autoreTemp.setIdAutore(listaAutori.get(k).getIdAutore());
					libro.setElencoAutori(autoreTemp);
				}
				else {
					inserAutore(autore[0], autore[1]);
				}
			}
		}
		Connection conn = Connector.getIstance().getConnection();
		String queryInserimento = "INSERT INTO libro (CodiceUnivoco,Titolo) VALUE (?,?)";
		PreparedStatement ps = conn.prepareStatement(queryInserimento, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, libro.getCodiceUnivoco());
		ps.setString(2, libro.getTitolo());
		ps.executeUpdate();

		ResultSet result = ps.getGeneratedKeys();
		result.next();
		libro.setIdLibro(result.getInt(1));

		if (libro.getIdLibro() > 0)
			return true;
		else
			return false;
	}

	private boolean addAutore(String nome, String cognome) throws SQLException {
		Autore autore = new Autore();
		autore.setNome(nome);
		autore.setCognome(cognome);

		Connection conn = Connector.getIstance().getConnection();
		String queryInserimento = "INSERT INTO autore (Nome,Cognome) VALUE (?,?)";
		PreparedStatement ps = conn.prepareStatement(queryInserimento, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, autore.getNome());
		ps.setString(2, autore.getCognome());
		ps.executeUpdate();
		ResultSet result = ps.getGeneratedKeys();
		result.next();
		autore.setIdAutore(result.getInt(1));

		if (autore.getIdAutore() > 0)
			return true;
		else
			return false;
	}

	private boolean addCategoria(String nomeCategoria) throws SQLException {
		Categoria categoria = new Categoria();
		categoria.setNomeCategoria(nomeCategoria);

		Connection conn = Connector.getIstance().getConnection();
		String queryInserimento = "INSERT INTO categoria (nomecategoria) VALUE (?)";
		PreparedStatement ps = conn.prepareStatement(queryInserimento, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, categoria.getNomeCategoria());
		ps.executeUpdate();
		ResultSet result = ps.getGeneratedKeys();
		result.next();
		categoria.setIdCategoria(result.getInt(1));

		if (categoria.getIdCategoria() > 0)
			return true;
		else
			return false;
	}

	private boolean addPrestito(Iscritto iscritto, String numeroPrestito, ArrayList<Libro> libriSelezionati)
			throws SQLException {
		Prestito prestito = new Prestito();
		prestito.setIscritto(iscritto);
		prestito.setNumeroPrestito(numeroPrestito);
		prestito.setElencoLibri(libriSelezionati);
		prestito.setDataInizio();
		prestito.setDataFine(null);
		int idIscritto = iscritto.getIdIscritto();

		Date dataInizio = (Date) Date.from(prestito.getDataInizio().atZone(ZoneId.systemDefault()).toInstant());

		for (int i = 0; i < libriSelezionati.size(); i++) {
			Libro libro = libriSelezionati.get(i);
			int idLibro = libro.getIdLibro();
			Connection conn = Connector.getIstance().getConnection();
			String queryInserimento = "INSERT INTO prestito (ID_iscritto, ID_libro, num_prestito, DataInizio, DataFine) VALUE (?, ?, ?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(queryInserimento);
			ps.setInt(1, idIscritto);
			ps.setInt(2, idLibro);
			ps.setString(3, numeroPrestito);
			ps.setDate(4, dataInizio);
			ps.setDate(5, null);

			ps.executeUpdate();

		}
		return true;

	}

	private void setLibri() throws SQLException {
		listaLibri.clear();

		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT (ID_libro, CodiceUnivoco, Titolo) FROM libro";
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet result = ps.executeQuery();

		while (result.next()) {
			Libro libro = new Libro();
			libro.setIdLibro(result.getInt(1));
			libro.setCodiceUnivoco(result.getString(2));
			libro.setTitolo(result.getString(3));

			this.listaLibri.add(libro);
		}
	}

	private void setAutori() throws SQLException {
		listaAutori.clear();

		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT (ID_autore,Nome,Cognome) FROM autore";
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet result = ps.executeQuery();

		while (result.next()) {
			Autore autore = new Autore();
			autore.setIdAutore(result.getInt(1));
			autore.setNome(result.getString(2));
			autore.setCognome(result.getString(3));

			this.listaAutori.add(autore);
		}
	}

	private void setCategorie() throws SQLException {
		listaCategorie.clear();

		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT (ID_categoria, nomecategoria) FROM categoria";
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet result = ps.executeQuery();

		while (result.next()) {
			Categoria categoria = new Categoria();
			categoria.setIdCategoria(result.getInt(1));
			categoria.setNomeCategoria(result.getString(2));

			this.listaCategorie.add(categoria);
		}
	}

	private void setIscritti() throws SQLException {
		listaIscritti.clear();

		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT (ID_iscritto,Nome,Cognome,cf,idTessera) FROM iscritto";
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet result = ps.executeQuery();

		while (result.next()) {
			Iscritto iscritto = new Iscritto();
			iscritto.setIdIscritto(result.getInt(1));
			iscritto.setNome(result.getString(2));
			iscritto.setCognome(result.getString(3));
			iscritto.setCodFiscale(result.getString(4));
			iscritto.setIdTessera();

			this.listaIscritti.add(iscritto);
		}
	}

	private boolean eliminaIscritto(String codiceFiscale) throws SQLException {
			Connection conn = Connector.getIstance().getConnection();
			String query_delete = "DELETE FROM iscritto WHERE CF = ?";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query_delete);

			ps.setString(1, codiceFiscale);

			int risultato_operazione = ps.executeUpdate();

			if (risultato_operazione > 0) {
				return true;
			} else {
				return false;
			}
	}

}
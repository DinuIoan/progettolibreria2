package com;

import java.sql.Connection;
import java.sql.SQLException;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class Connector {
	private Connection conn;
	private static Connector connection;
	
	public static Connector getIstance() {
		if(connection == null) {
			connection = new Connector();
			return connection;
		}
		else
			return connection;
	}
	
	public Connection getConnection() throws SQLException {
		if(conn == null) {
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName("127.0.0.1");
			dataSource.setPort(3306);
			dataSource.setUser("root");
			dataSource.setPassword("Mujmafoqpab339!");
			dataSource.setUseSSL(false);
			dataSource.setDatabaseName("homework");
			
			conn = dataSource.getConnection();
			return conn;
		}
		else
			return conn;
	}
}
